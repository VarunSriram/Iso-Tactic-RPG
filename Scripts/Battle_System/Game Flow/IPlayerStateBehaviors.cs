﻿using Game.BattleSystem.UI;
using Game.BattleSystem.Units.BoardUnits;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public interface IPlayerTurnStateBehaviours
    {
        void playerTurnStart(PlayerFSM fsm);
        void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui);
        void moveCommandPicked(PlayerFSM fsm);
        void cancelMovement(PlayerFSM fsm);
        void moveUnit(PlayerFSM fsm);
        void attackCommandPicked(PlayerFSM fsm);
        void cancelAttack(PlayerFSM fsm);
        void unitAttack(PlayerFSM fsm);
        void playerTurnEnd(PlayerFSM fsm);
    }
}