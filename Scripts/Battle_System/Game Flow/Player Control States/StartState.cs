﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.StateMachines;
using Game.Utility;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class PlayerStartState : PlayerTurnState
    {
        private PlayerStartState() { }

        private static PlayerStartState INSTANCE = new PlayerStartState();

        public static PlayerStartState instance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new PlayerStartState();
                return INSTANCE;
            }
            return INSTANCE;
        }

        public override void onStateEnter(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Entering Start State", GetHashCode(), GetType());
        }

        public override void onStateExit(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Exiting Start State", GetHashCode(), GetType());
        }

        public override void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui)
        {
            throw new NotImplementedException();
        }

        public override void attackCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveUnit(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void unitAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnStart(PlayerFSM fsm)
        {
            //TODO: Player Turn Start Action here...
            //TODO: Select random player unit on the board.
            DebugLogger.instance.logInfo("Starting Players Turn", GetHashCode(), GetType());
            fsm.setState(PlayerUnitSelectState.instance());
        }

        public override void playerTurnEnd(PlayerFSM fsm)
        {
            //TODO: do something to flag the turn hass ended.
            fsm.setState(EndState.instance());
        }

        public override void cancelAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelMovement(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }
    }
}