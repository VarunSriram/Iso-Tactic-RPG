﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Utility;
using Game.StateMachines;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class PlayerUnitSelectState : PlayerTurnState
    {

        private PlayerUnitSelectState() { }

        private static PlayerUnitSelectState INSTANCE = new PlayerUnitSelectState();

        public static PlayerUnitSelectState instance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new PlayerUnitSelectState();
                return INSTANCE;
            }
            return INSTANCE;
        }

        public override void onStateEnter(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Entering Unit Select State", GetHashCode(), GetType());
        }

        public override void onStateExit(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Exiting Unit Select State", GetHashCode(), GetType());
        }

        public override void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui)
        {
            DebugLogger.instance.logInfo("Exiting Unit Select State", GetHashCode(), GetType());
            //TODO "Update the UI based on the unit selected"
            //Note* might remove this method all to gether and put this logic in the enter state method.
            ui.showUnitInformation(selectedUnit);
            ui.showPlayerIdleCommands();
            fsm.setState(PlayerUnitSelectState.instance());
        }

        public override void attackCommandPicked(PlayerFSM fsm)
        {
            //TODO Show attack range/possible targets in the UI
            //TOOO change attack Button to cancel attack
            fsm.setState(SelectPlayerAttackTarget.instance());
        }

        public override void moveCommandPicked(PlayerFSM fsm)
        {
            //TODO Show possible movement spots in the UI
            //TOOO change move Button to cancel attack
            fsm.setState(SelectPlayerMovementTarget.instance());
        }

        public override void moveUnit(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void unitAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnStart(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnEnd(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelMovement(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

    }
}
