﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.Utility;
using Game.StateMachines;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class SelectPlayerAttackTarget : PlayerTurnState
    {
        private SelectPlayerAttackTarget() { }

        private static SelectPlayerAttackTarget INSTANCE = new SelectPlayerAttackTarget();

        public static SelectPlayerAttackTarget instance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new SelectPlayerAttackTarget();
                return INSTANCE;
            }
            return INSTANCE;
        }

        public override void onStateEnter(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Entering Attack Target State", GetHashCode(), GetType());
        }

        public override void onStateExit(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Exiting Attack Target State", GetHashCode(), GetType());
        }

        public override void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui)
        {
            throw new NotImplementedException();
        }

        public override void attackCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveUnit(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void unitAttack(PlayerFSM fsm)
        {
            //TODO Hide attack range/possible targets in the UI
            //TODO play attack animation
            //TODO update player and enemy stats / prevent unit from attacking again
            fsm.setState(PlayerUnitSelectState.instance());
        }

        public override void playerTurnStart(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnEnd(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelAttack(PlayerFSM fsm)
        {
            //TODO Hide attack range/possible targets in the UI
            //TOOO cancel Button to attack
            fsm.setState(PlayerUnitSelectState.instance());
        }

        public override void cancelMovement(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }
    }
}