﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.StateMachines;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public abstract class PlayerTurnState : State, IPlayerTurnStateBehaviours
    {
        public abstract void attackCommandPicked(PlayerFSM fsm);

        public abstract void cancelAttack(PlayerFSM fsm);

        public abstract void moveCommandPicked(PlayerFSM fsm);

        public abstract void cancelMovement(PlayerFSM fsm);

        public abstract void moveUnit(PlayerFSM fsm);

        public abstract void playerTurnEnd(PlayerFSM fsm);

        public abstract void playerTurnStart(PlayerFSM fsm);

        public abstract void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui);

        public abstract void unitAttack(PlayerFSM fsm);

    }
}
