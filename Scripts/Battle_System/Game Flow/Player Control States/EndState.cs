﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.StateMachines;
using Game.Utility;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class EndState : PlayerTurnState
    {

        private EndState() { }

        private static EndState INSTANCE = new EndState();

        public static EndState instance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new EndState();
                return INSTANCE;
            }
            return INSTANCE;
        }

        public override void onStateEnter(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Entering End State", GetHashCode(), GetType());
        }

        public override void onStateExit(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Exiting End State", GetHashCode(), GetType());
        }

        public override void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui)
        {
            throw new NotImplementedException();
        }

        public override void attackCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveUnit(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void unitAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnStart(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnEnd(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelMovement(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }
    }
}
