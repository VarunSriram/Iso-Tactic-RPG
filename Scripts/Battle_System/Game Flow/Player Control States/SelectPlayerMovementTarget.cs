﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.Utility;
using Game.StateMachines;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class SelectPlayerMovementTarget : PlayerTurnState
    {
        private SelectPlayerMovementTarget() { }

        private static SelectPlayerMovementTarget INSTANCE = new SelectPlayerMovementTarget();

        public static SelectPlayerMovementTarget instance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new SelectPlayerMovementTarget();
                return INSTANCE;
            }
            return INSTANCE;
        }

        public override void onStateEnter(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Entering Unit Select State", GetHashCode(), GetType());
        }

        public override void onStateExit(StateMachine fsm)
        {
            DebugLogger.instance.logInfo("Exiting Unit Select State", GetHashCode(), GetType());
        }

        public override void selectUnit(PlayerFSM fsm, AbstractBoardUnit selectedUnit, UIView ui)
        {
            throw new NotImplementedException();
        }

        public override void attackCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveCommandPicked(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void moveUnit(PlayerFSM fsm)
        {
            //TODO Hide movement targets in the UI
            //TODO play movement animation
            //TODO update player and enemy stats / prevent unit from moving again if the unit is out of movement points.
            fsm.setState(PlayerUnitSelectState.instance());
        }

        public override void unitAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnStart(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void playerTurnEnd(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelAttack(PlayerFSM fsm)
        {
            throw new NotImplementedException();
        }

        public override void cancelMovement(PlayerFSM fsm)
        {
            //TODO Hide movement options in the UI
            //TOOO change cancel Button to move
            fsm.setState(PlayerUnitSelectState.instance());
        }

    }
}