﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameFlow.PlayerStateMachine;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.Components {
    public class PlayerControlStateMachine : MonoBehaviour {
        [SerializeField]
        private PlayerFSM playerStateMachine;
    
        public void initializeStateMachine()
        {
            playerStateMachine.initialize();
        }

        public void performUnitSelection(AbstractBoardUnit selectedUnit, UIView ui)
        {
            playerStateMachine.playerSelectUnit(selectedUnit, ui);
        }

        public void initializeMovementCommand()
        {
            playerStateMachine.playerInvokeMovementCommand();
        }

        public void cancelMovementCommand()
        {
            playerStateMachine.playerCancelMovementCommand();
        }

        public void moveUnit()
        {
            playerStateMachine.playerMoveUnit();
        }

        public void initializeAttackCommand()
        {
            playerStateMachine.playerInvokeAttackCommand();
        }

        public void cancelAttackCommand()
        {
            playerStateMachine.playerCancelAttackCommand();
        }

        public void attack()
        {
            playerStateMachine.playerUnitAttack();
        }

        public void endTurn() {
            playerStateMachine.playerEndTurn();
        }

        public string getCurrentStateName()
        {
            return playerStateMachine.getCurrentState().GetType().Name;
        }
    }
}