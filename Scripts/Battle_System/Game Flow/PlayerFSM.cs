﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Utility;
using Game.StateMachines;
using System;
using Game.BattleSystem.Units.BoardUnits;
using Game.BattleSystem.UI;

namespace Game.BattleSystem.GameFlow.PlayerStateMachine
{
    public class PlayerFSM : StateMachine
    {

        public void playerTurnStart()
        {
            DebugLogger.instance.logInfo("Player Turn started", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.playerTurnStart(this);
        }

        public void playerSelectUnit(AbstractBoardUnit selectedUnit, UIView ui)
        {
            DebugLogger.instance.logInfo("Player Selected Unit", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.selectUnit(this,selectedUnit,ui);
        }

        public void playerInvokeMovementCommand()
        {
            DebugLogger.instance.logInfo("Player chose to move unit", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.moveCommandPicked(this);
        }

        public void playerCancelMovementCommand()
        {
            DebugLogger.instance.logInfo("Player cancelled move unit", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.cancelMovement(this);
        }

        public void playerMoveUnit()
        {
            DebugLogger.instance.logInfo("Player Moving Unit", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.moveUnit(this);
        }

        public void playerInvokeAttackCommand()
        {
            DebugLogger.instance.logInfo("Player chose for unit to attack", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.attackCommandPicked(this);
        }

        public void playerCancelAttackCommand()
        {
            DebugLogger.instance.logInfo("Player cancel unit attack", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.cancelAttack(this);
        }

        public void playerUnitAttack()
        {
            DebugLogger.instance.logInfo("Player Unit Attacking", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.unitAttack(this);
        }

        public void playerEndTurn()
        {
            DebugLogger.instance.logInfo("Player Ending Turn", GetHashCode(), GetType());
            PlayerTurnState pts = (PlayerTurnState)this.currentState;
            pts.playerTurnEnd(this);
        }

        public override void initialize()
        {
            this.currentState = PlayerStartState.instance();
        }
    }
}