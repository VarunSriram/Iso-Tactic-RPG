﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.Units.Statistics
{
    public class UnitStatistics : MonoBehaviour
    {

        [SerializeField]
        protected int currentUnitHitPoints = 1;
        [SerializeField]
        protected int maxUnitHitPoints = 1;

        [SerializeField]
        protected int currentMovementPoints = 1;
        [SerializeField]
        protected int maxMovementPoints = 1;

        public int CurrentHP
        {
            get { return this.currentUnitHitPoints; }
            set
            {
                if (value < 0)
                    currentUnitHitPoints = 0;
                else if (value > maxUnitHitPoints)
                    currentUnitHitPoints = maxUnitHitPoints;
                else
                    currentUnitHitPoints = value;
            }
        }

        public int MaxHP
        {
            get { return this.currentUnitHitPoints; }
            set
            {
                if (value <= 0)
                    maxUnitHitPoints = 1;
                else if (value < currentUnitHitPoints)
                    maxUnitHitPoints = currentUnitHitPoints;
                else
                    maxUnitHitPoints = value;
            }
        }

        public int MovementPoints
        {
            get { return this.currentMovementPoints; }
            set
            {
                if (value < 0)
                    currentMovementPoints = 0;
                else if (value > maxUnitHitPoints)
                    currentMovementPoints = maxMovementPoints;
                else
                    currentMovementPoints = value;
            }
        }

        public int MaxMovementPoints
        {
            get { return this.maxMovementPoints; }
            set
            {
                if (value <= 0)
                    maxMovementPoints = 1;
                else if (value < currentMovementPoints)
                    maxMovementPoints = currentMovementPoints;
                else
                    maxMovementPoints = value;
            }
        }

        public void intializeStatistics(int maxHP, int maxMP)
        {
            this.currentUnitHitPoints = maxHP;
            this.maxUnitHitPoints = maxHP;
            this.currentMovementPoints = maxMP;
            this.maxMovementPoints = maxMP;
        }
    }
}