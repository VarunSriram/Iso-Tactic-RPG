﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.Units.Statistics
{
    public class VehicleStatistics : UnitStatistics
    {
        [SerializeField]
        protected int armor;
        [SerializeField]
        protected int currentElectroMagPoints = 1;
        [SerializeField]
        protected int maxElectroMagPoints = 1;

        public void intializeStatistics(int maxHP, int maxMP, int armor, int maxEMP)
        {
            base.intializeStatistics(maxHP, maxMP);
            this.currentUnitHitPoints = maxHP;
            this.armor = armor;
            this.maxElectroMagPoints = maxEMP;
            this.currentElectroMagPoints = maxEMP;
        }

        public int ElectroMag
        {
            get { return this.currentElectroMagPoints; }
            set
            {
                if (value < 0)
                    currentElectroMagPoints = 0;
                else if (value > maxElectroMagPoints)
                    currentElectroMagPoints = maxElectroMagPoints;
                else
                    currentElectroMagPoints = value;
            }
        }

        public int MaxElectroMagPoints
        {
            get { return this.maxElectroMagPoints; }
            set
            {
                if (value <= 0)
                    maxElectroMagPoints = 1;
                else if (value < currentElectroMagPoints)
                    maxElectroMagPoints = currentMovementPoints;
                else
                    maxElectroMagPoints = value;
            }
        }

        public int Armor
        {
            get { return this.armor; }
            set
            {
                if (value < 0)
                    this.armor = 0;
                else
                    this.armor = value;
            }
        }
    }
}