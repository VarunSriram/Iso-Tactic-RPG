﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.Units.UnitPlacement;
using Game.BattleSystem.Units.Statistics;
namespace Game.BattleSystem.Units.BoardUnits
{
    public abstract class AbstractBoardUnit : MonoBehaviour
    {

        protected string unitName;
        protected AbstractUnitPlacement unitPlacement;
        [SerializeField]
        protected bool isAirUnit = false;
        [SerializeField]
        protected UnitStatistics unitStatistics;

        // Use this for initialization
        public abstract void Awake();

        public Vector2 origin()
        {
            return unitPlacement.Origin;
        }

        public CompassDirection directionFacing()
        {
            return unitPlacement.DirectionFacing;
        }

        public void moveUnit(Vector2 dest)
        {
            this.unitPlacement.move(dest);
        }

        public void rotateUnit(CompassDirection dir)
        {
            this.unitPlacement.turn(dir);
        }

        public List<Vector2> getBoardSpace()
        {
            return this.unitPlacement.getOccupyingSpace();
        }

        public List<Vector2> getBoardSpace(Vector2 coord)
        {
            return this.unitPlacement.getOccupyingSpace(coord);
        }

        public bool IsAirUnit
        {
            get { return this.isAirUnit; }
        }

        public string UnitName
        {
            get { return this.unitName; }
            set { this.unitName = value; }
        }

        public UnitStatistics Stats
        {
            get { return this.unitStatistics; }
            set { this.unitStatistics = value; }
        }
    }
}