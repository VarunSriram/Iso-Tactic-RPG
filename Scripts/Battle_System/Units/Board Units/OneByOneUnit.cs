﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.Units.UnitPlacement;
namespace Game.BattleSystem.Units.BoardUnits
{
    public class OneByOneUnit : AbstractBoardUnit
    {
        public override void Awake()
        {
            this.unitPlacement = new OneByOneUnitPlacement();
        }
    }
}