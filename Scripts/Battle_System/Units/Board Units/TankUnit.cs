﻿using Game.BattleSystem.Units.Statistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.BattleSystem.Units.BoardUnits
{
    
    public class TankUnit : TwoByTwoUnit
    {
        public override void Awake()
        {
            base.Awake();
            VehicleStatistics vStats = (VehicleStatistics)this.unitStatistics;
            vStats.intializeStatistics(100,100,100,100);

        }

    }
}