﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.Units.UnitPlacement;
using Game.BattleSystem.Units.Statistics;

namespace Game.BattleSystem.Units.BoardUnits
{
    public class TwoByTwoUnit : AbstractBoardUnit
    {
        public override void Awake()
        {
            this.unitPlacement = new TwoByTwoUnitPlacement();
            this.unitStatistics = GetComponent<UnitStatistics>();
        }
    }
}