﻿using Game.Controls.TileInteractions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.BattleSystem.Units.BoardUnits;

public class UnitMouseInteraction : MouseClickInteraction
{
    [SerializeField]
    private BoxCollider clickCollider;
    [SerializeField]
    private AbstractBoardUnit unit;

    void Awake()
    {
        clickCollider = GetComponent<BoxCollider>();
        unit = GetComponent<AbstractBoardUnit>(); 
    }

    public override void clickAction()
    {
        throw new NotImplementedException();
    }

    public bool isClickable()
    {
        return clickCollider.enabled;
    }

    public void setClickColliderOn(bool setting)
    {
        this.clickCollider.enabled = setting;
    }


}
