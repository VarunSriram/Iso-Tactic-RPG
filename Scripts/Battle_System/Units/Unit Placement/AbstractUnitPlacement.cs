﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.BattleSystem.Units.UnitPlacement
{
    public abstract class AbstractUnitPlacement
    {
        protected List<Vector2> shapeDescription;
        protected Vector2 origin;
        protected CompassDirection directionFacing = Compass.NORTH;

        public AbstractUnitPlacement()
        {
            this.origin = new Vector2(0, 0);
            this.defineShape();
        }

        public abstract void defineShape();

        public void move(Vector2 dest)
        {
            this.origin = dest;
        }

        public void turn(CompassDirection dir)
        {
            this.directionFacing = dir;
        }

        public List<Vector2> getOccupyingSpace()
        {
            List<Vector2> vectorList = new List<Vector2>();
            vectorList.Add(this.origin);
            foreach (Vector2 v2 in this.shapeDescription)
            {
                vectorList.Add(v2 + origin);
            }

            return vectorList;

        }

        public List<Vector2> getOccupyingSpace(Vector2 vector)
        {
            List<Vector2> vectorList = new List<Vector2>();
            vectorList.Add(vector);
            foreach (Vector2 v2 in this.shapeDescription)
            {
                vectorList.Add(v2 + vector);
            }

            return vectorList;
        }

        public Vector2 Origin
        {
            get { return origin; }
        }
        public CompassDirection DirectionFacing
        {
            get { return directionFacing; }
        }

    }
}