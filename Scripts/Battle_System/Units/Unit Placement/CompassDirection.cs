﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.BattleSystem.Units.UnitPlacement
{

    public class CompassDirection
    {
        private string name;
        private Vector2 dirRepresentation;
        private Quaternion rotRepresentation;


        public CompassDirection(string name, Vector2 dir, Quaternion rot)
        {
            this.name = name;
            this.dirRepresentation = dir;
            this.rotRepresentation = rot;
        }

        public string Name { get { return this.name; } set { name = value; } }
        public Vector2 DirRepresentation { get { return this.dirRepresentation; } set { dirRepresentation = value; } }
        public Quaternion RotRepresentation { get { return this.rotRepresentation; } set { rotRepresentation = value; } }
    }
}