﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.Units.UnitPlacement
{
    public class OneByOneUnitPlacement : AbstractUnitPlacement
    {

        public OneByOneUnitPlacement() : base()
        {

        }

        public override void defineShape()
        {
            this.shapeDescription = new List<Vector2>();
        }

    }
}