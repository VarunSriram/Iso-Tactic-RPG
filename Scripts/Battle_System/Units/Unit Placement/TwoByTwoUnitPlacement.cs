﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.Units.UnitPlacement
{
    public class TwoByTwoUnitPlacement : AbstractUnitPlacement
    {
        public TwoByTwoUnitPlacement() : base()
        {

        }

        public override void defineShape()
        {
            this.shapeDescription = new List<Vector2>();
            this.shapeDescription.Add(new Vector2(0, -1));
            this.shapeDescription.Add(new Vector2(-1, 0));
            this.shapeDescription.Add(new Vector2(-1, -1));
        }
    }
}