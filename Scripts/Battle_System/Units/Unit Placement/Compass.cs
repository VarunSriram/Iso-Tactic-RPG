﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.Units.UnitPlacement
{
    public static class Compass
    {
        public static CompassDirection NORTH { get { return new CompassDirection("north", new Vector2(0, 1), Quaternion.Euler(new Vector3(0, 0, 90))); } }
        public static CompassDirection EAST { get { return new CompassDirection("east", new Vector2(1, 0), Quaternion.Euler(new Vector3(0, 0, 0))); } }
        public static CompassDirection WEST { get { return new CompassDirection("west", new Vector2(-1, 0), Quaternion.Euler(new Vector3(0, 0, 180))); } }
        public static CompassDirection SOUTH { get { return new CompassDirection("south", new Vector2(0, -1), Quaternion.Euler(new Vector3(0, 0, 270))); } }

        public static CompassDirection NORTHWEST { get { return new CompassDirection("northwest", new Vector2(-1, 1), Quaternion.Euler(new Vector3(0, 0, 135))); } }
        public static CompassDirection NORTHEAST { get { return new CompassDirection("northeast", new Vector2(1, 1), Quaternion.Euler(new Vector3(0, 0, 45))); } }
        public static CompassDirection SOUTHEAST { get { return new CompassDirection("southeast", new Vector2(1, -1), Quaternion.Euler(new Vector3(0, 0, 315))); } }
        public static CompassDirection SOUTHWEST { get { return new CompassDirection("southwest", new Vector2(-1, -1), Quaternion.Euler(new Vector3(0, 0, 225))); } }

    }
}