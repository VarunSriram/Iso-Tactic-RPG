﻿using Game.BattleSystem.Units.BoardUnits;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Game.BattleSystem.UI
{
    public class UIView : MonoBehaviour
    {
        [SerializeField]
        private Button moveCommandButton;
        [SerializeField]
        private Button attackCommandButton;
        [SerializeField]
        private Button cancelAttackButton;
        [SerializeField]
        private Button cancelMoveButton;
        [SerializeField]
        private Button endTurnButton;
        [SerializeField]
        private Text unitLabel;
        [SerializeField]
        private Image unitPortrait;

        //TODO take in some class representing a unit on the board, and 
        //update the view,
        public void showUnitInformation(AbstractBoardUnit abu)
        {
            unitPortrait.color = Color.magenta;
            //TODO health bars stamina and EM bars, involves differentiating between units.
            this.setUnitLabel (abu.UnitName);
        }

        public void enableMoveCommand()
        {
            this.hideAttackCommandButton();
            this.hideCancelAttackCommandButton();
            this.hideCancelMoveCommandButton();
            this.showMoveCommandButton();
            this.disableEndTurnButton();
        }

        public void enableCancelMoveCommand()
        {
            this.hideAttackCommandButton();
            this.hideMoveCommandButton();
            this.showCancelMoveCommandButton();
            this.disableEndTurnButton();
        }

        public void enableAttackCommand()
        {
            this.showAttackCommandButton();
            this.hideCancelAttackCommandButton();
            this.hideCancelMoveCommandButton();
            this.hideMoveCommandButton();
            this.disableEndTurnButton();
        }

        public void enableCancelAttackCommand()
        {
            this.hideAttackCommandButton();
            this.hideMoveCommandButton();
            this.showCancelMoveCommandButton();
            this.disableEndTurnButton();
        }

        public void showPlayerIdleCommands()
        {
            this.hideCancelAttackCommandButton();
            this.hideCancelMoveCommandButton();
            this.showMoveCommandButton();
            this.showAttackCommandButton();
            this.enableEndTurnButton();
        }

        private void hideMoveCommandButton()
        {
            this.moveCommandButton.interactable = false;
            this.moveCommandButton.enabled = false;
        }

        private void showMoveCommandButton()
        {
            this.moveCommandButton.interactable = true;
            this.moveCommandButton.enabled = true;
        }

        private void hideAttackCommandButton()
        {
            this.attackCommandButton.interactable = false;
            this.attackCommandButton.enabled = false;
        }

        private void showAttackCommandButton()
        {
            this.attackCommandButton.interactable = true;
            this.attackCommandButton.enabled = true;
        }

        private void showCancelMoveCommandButton()
        {
            this.cancelMoveButton.interactable = true;
            this.cancelMoveButton.enabled = true;
        }

        private void hideCancelMoveCommandButton()
        {
            this.cancelMoveButton.interactable = false;
            this.cancelMoveButton.enabled = false;
        }

        private void showCancelAttackCommandButton()
        {
            this.cancelAttackButton.interactable = true;
            this.cancelAttackButton.enabled = true;
        }

        private void hideCancelAttackCommandButton()
        {
            this.cancelAttackButton.interactable = false;
            this.cancelAttackButton.enabled = false;
        }

        private void enableEndTurnButton()
        {
            this.endTurnButton.interactable = true;
        }

        private void disableEndTurnButton()
        {
            this.endTurnButton.interactable = false;
        }

        private void setUnitLabel(string s)
        {
            this.unitLabel.text = s;
        }
    }
}