﻿using Game.BattleSystem.GameFlow.Components;
using Game.BattleSystem.GameFlow.PlayerStateMachine;
using Game.BattleSystem.UI;
using Game.BattleSystem.Units.BoardUnits;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
    [SerializeField]
    private PlayerControlStateMachine stateMachineModel;
    [SerializeField]
    private UIView uiView;
    [SerializeField]
    private string stateMachineCurrentState;


    void Awake()
    {
        this.stateMachineModel = GetComponent<PlayerControlStateMachine>();
        uiView = GetComponent<UIView>();
        stateMachineModel.initializeStateMachine();
        this.stateMachineCurrentState = stateMachineModel.getCurrentStateName();
    }

    void selectUnit(AbstractBoardUnit abu)
    {
        stateMachineModel.performUnitSelection(abu,uiView);
    }


}
