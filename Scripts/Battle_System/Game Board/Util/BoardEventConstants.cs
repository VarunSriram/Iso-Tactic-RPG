﻿using System.Collections;
using System.Collections.Generic;

namespace Game.BattleSystem.GameBoard.Util
{
    public class BoardEventConstants
    {
        public static string showGroundSpace = "showGroundStatus";
        public static string showAirSpace = "showAirStatus";
        public static string hideGroundSpace = "hideGroundSpace";
        public static string hideAirSpace = "hideAirSpace";
        public static string showGroundMovementSpace = "showGroundMovement";
        public static string hideGroundMovementSpace = "hideGroundMovement";
        public static string showAirMovementSpace = "showAirMovement";
        public static string hideAirMovementSpace = "hideAirMovement";
    }
}