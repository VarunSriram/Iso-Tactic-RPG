﻿using System.Collections;
using System.Collections.Generic;
namespace Game.BattleSystem.GameBoard.Util
{
    public static class TileConstants
    {
        public static string emptyTileName = "empty";
        public static string squareTileName = "tile";
        public static string eastSideTileName = "east_side";
        public static string northSideTileName = "north_side";
        public static string southSideTileName = "south_side";
        public static string westSideTileName = "west_side";

        public static string northEastCurveTileName = "north_east_curve";
        public static string northWestCurveTileName = "north_west_curve";
        public static string southEastCurveTileName = "south_east_curve";
        public static string southWestCurveTileName = "south_west_curve";

        public static string northEastCornerTileName = "north_east_corner";
        public static string northWestCornerTileName = "north_west_corner";
        public static string southEastCornerTileName = "south_east_corner";
        public static string southWestCornerTileName = "south_west_corner";


        public static string hillSquareTileName = "tile_hill";
        public static string hillEastSideTileName = "east_side_hill";
        public static string hillNorthSideTileName = "north_side_hill";
        public static string hillSouthSideTileName = "south_side_hill";
        public static string hillWestSideTileName = "west_side_hill";

        public static string hillNorthEastCurveTileName = "north_east_curve_hill";
        public static string hillNorthWestCurveTileName = "north_west_curve_hill";
        public static string hillSouthEastCurveTileName = "south_east_curve_hill";
        public static string hillSouthWestCurveTileName = "south_west_curve_hill";

        public static string hillNorthEastCornerTileName = "north_east_corner_hill";
        public static string hillNorthWestCornerTileName = "north_west_corner_hill";
        public static string hillSouthEastCornerTileName = "south_east_corner_hill";
        public static string hillSouthWestCornerTileName = "south_west_corner_hill";




        public static string squareSymbol = "0";
        public static string eastSideSymbol = "6";
        public static string northSideSymbol = "7";
        public static string southSideSymbol = "8";
        public static string westSideSymbol = "5";

        public static string northEastCurveSymbol = "2";
        public static string northWestCurveSymbol = "1";
        public static string southEastCurveSymbol = "4";
        public static string southWestCurveSymbol = "3";

        public static string northEastCornerSymbol = "#";
        public static string northWestCornerSymbol = "@";
        public static string southEastCornerSymbol = "$";
        public static string southWestCornerSymbol = "%";

        public static string hillSquareSymbol = "+";
        public static string hillEastSideSymbol = "]";
        public static string hillNorthSideSymbol = "M";
        public static string hillSouthSideSymbol = "W";
        public static string hillWestSideSymbol = "[";

        public static string hillNorthEastCurveSymbol = "}";
        public static string hillNorthWestCurveSymbol = "{";
        public static string hillSouthEastCurveSymbol = ">";
        public static string hillSouthWestCurveSymbol = "<";

        public static string hillNorthEastCornerSymbol = "C";
        public static string hillNorthWestCornerSymbol = "G";
        public static string hillSouthEastCornerSymbol = "U";
        public static string hillSouthWestCornerSymbol = "I";



        public static string emptySymbol = "_";

        public static string tilePassivesObjName = "Passive Effects";
        public static string tallGrassPassiveEffect = "Grass";
        public static string grassSymbol = "^";
        public static string walkableStatus = "walkable";
        public static string nonWalkableStatus = "non-walkable";
        public static string flyableStatus = "flyable";
        public static string nonFlyableStatus = "non-flyable";
        public static string groundMovement = "ground-movement";
        public static string airMovement = "air-movement";

        public static bool isHillSymbol(string symbol)
        {
            return symbol.Equals(hillSquareSymbol) ||
                   symbol.Equals(hillEastSideSymbol) ||
                   symbol.Equals(hillNorthSideSymbol) ||
                   symbol.Equals(hillSouthSideSymbol) ||
                   symbol.Equals(hillWestSideSymbol) ||

                   symbol.Equals(hillNorthEastCurveSymbol) ||
                   symbol.Equals(hillNorthWestCurveSymbol) ||
                   symbol.Equals(hillSouthEastCurveSymbol) ||
                   symbol.Equals(hillSouthWestCurveSymbol) ||

                   symbol.Equals(hillNorthEastCornerSymbol) ||
                   symbol.Equals(hillNorthWestCornerSymbol) ||
                   symbol.Equals(hillSouthEastCornerSymbol) ||
                   symbol.Equals(hillSouthWestCornerSymbol);

        }
    }
}