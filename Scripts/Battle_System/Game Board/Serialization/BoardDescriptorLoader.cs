﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Utility;

namespace Game.BattleSystem.GameBoard.Serialization
{
    public class BoardDescriptorLoader : MonoBehaviour
    {
        private Dictionary<string, TextAsset> descriptorManifest;

        [SerializeField]
        private string resourceLoc = "Board Descriptors";
        [SerializeField]
        private bool debugMode = true;

        void Awake()
        {
            descriptorManifest = new Dictionary<string, TextAsset>();
            TextAsset[] discriptors = Resources.LoadAll<TextAsset>(resourceLoc);
            foreach (TextAsset ta in discriptors)
            {
                descriptorManifest.Add(ta.name, ta);
            }
            if (debugMode)
            {
                DebugLogger.instance.logInfo(descriptorManifest.Keys.Count == 0 ? "No descriptors found" : "Descriptor Files found", GetHashCode(), GetType(), gameObject.name);
                foreach (string s in descriptorManifest.Keys)
                {
                    DebugLogger.instance.logInfo(s, GetHashCode(), GetType(), gameObject.name);
                }
            }
        }

        public string getDescriptorFileText(string descriptorName)
        {
            return descriptorManifest[descriptorName].text;
        }

    }
}