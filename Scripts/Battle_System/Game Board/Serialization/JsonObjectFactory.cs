﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Game.BattleSystem.GameBoard.Serialization
{
    public abstract class JsonObjectFactory
    {
        public abstract ISerializedJsonObj createInstance(string jsonText);
        public abstract Boolean verifyJsonObject(ISerializedJsonObj jsonObj);
    }

}