﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Game.BattleSystem.GameBoard.Serialization.Domain;
using Game.BattleSystem.GameBoard.Util;

namespace Game.BattleSystem.GameBoard.Serialization.Factories
{
    public class BoardModelFactory : JsonObjectFactory
    {
        private List<String> tileSymbols;

        public BoardModelFactory()
        {
            string[] symbols ={
                        TileConstants.squareSymbol,
                        TileConstants.northSideSymbol,
                        TileConstants.southSideSymbol,
                        TileConstants.eastSideSymbol,
                        TileConstants.westSideSymbol,
                        TileConstants.northWestCurveSymbol,
                        TileConstants.northEastCurveSymbol,
                        TileConstants.southWestCurveSymbol,
                        TileConstants.southEastCurveSymbol,
                        TileConstants.northWestCornerSymbol,
                        TileConstants.northEastCornerSymbol,
                        TileConstants.southWestCornerSymbol,
                        TileConstants.southEastCornerSymbol,
                        TileConstants.hillSquareSymbol,
                        TileConstants.hillNorthSideSymbol,
                        TileConstants.hillSouthSideSymbol,
                        TileConstants.hillEastSideSymbol,
                        TileConstants.hillWestSideSymbol,
                        TileConstants.hillNorthWestCurveSymbol,
                        TileConstants.hillNorthEastCurveSymbol,
                        TileConstants.hillSouthWestCurveSymbol,
                        TileConstants.hillSouthEastCurveSymbol,
                        TileConstants.hillNorthWestCornerSymbol,
                        TileConstants.hillNorthEastCornerSymbol,
                        TileConstants.hillSouthWestCornerSymbol,
                        TileConstants.hillSouthEastCornerSymbol,
                        TileConstants.emptySymbol
                        };
            tileSymbols = new List<string>(symbols);
        }

        public override ISerializedJsonObj createInstance(string jsonText)
        {
            BoardModel bm = JsonUtility.FromJson<BoardModel>(jsonText);
            Boolean verification = this.verifyJsonObject(bm);
            if (verification)
                return bm;
            else
                throw new InvalidJsonDataException("Invalid Json Data Is Incorrect");
        }

        public override bool verifyJsonObject(ISerializedJsonObj jsonObj)
        {
            BoardModel bm = (BoardModel)jsonObj;
            foreach (String s in bm.tileData.tiles)
            {
                if (!tileSymbols.Contains(s))
                {
                    Debug.LogException(new InvalidJsonDataException("Invalid Tile Symbol " + s + " For This Board Please Verify Board Json File"));
                    return false;
                }


            }

            if (bm.tileData.tiles.Length != (bm.width * bm.height))
            {
                Debug.LogException(new InvalidJsonDataException("Invalid Number Of Tiles Given The Width and Height Of This Board Please Verify Board Json File " + " Width " + bm.width + " Height " + bm.height + "TileList Length: " + bm.tileData.tiles.Length));
                return false;
            }

            return true;
        }
    }

}