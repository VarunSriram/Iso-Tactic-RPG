﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.GameBoard.Serialization
{
    public interface ISerializedJsonObj
    {
        string getObjTypeName();
    }
}