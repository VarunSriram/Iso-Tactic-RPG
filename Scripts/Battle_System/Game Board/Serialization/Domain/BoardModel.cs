﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.BattleSystem.GameBoard.Serialization.Domain
{
    [System.Serializable]
    public class BoardModel : ISerializedJsonObj
    {
        public string boardName;
        public int width;
        public int height;
        public TileData tileData;

        public string getObjTypeName()
        {
            return "BoardModel";
        }
    }

    [System.Serializable]
    public class TileData
    {
        public string[] tiles;
        public string[] grassMap;
    }
}