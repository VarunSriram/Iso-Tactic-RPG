﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Game.BattleSystem.GameBoard.Serialization
{
    public class InvalidJsonDataException : Exception
    {
        public InvalidJsonDataException(string message) : base(message)
        {
        }
    }


}