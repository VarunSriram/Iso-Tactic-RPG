﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Game.BattleSystem.GameBoard.PathFinding
{
    public class PathFinding : MonoBehaviour
    {

        void Awake()
        {

        }

        public void findPath(PathRequest request, Action<PathResult> callback)
        {
            Vector2[] wayPoints = new Vector2[0];
            bool pathSuccess = false;

            Node startNode = request.pathGrid.nodeFromGridPosition(request.pathStart);
            Node targetNode = request.pathGrid.nodeFromGridPosition(request.pathEnd);

            if (startNode.traversable && targetNode.traversable)
            {
                Heap<Node> openSet = new Heap<Node>(request.pathGrid.MaxSize);
                HashSet<Node> closedSet = new HashSet<Node>();
                openSet.add(startNode);

                while (openSet.Count > 0)
                {
                    Node currentNode = openSet.removeFirst();
                    closedSet.Add(currentNode);

                    if (currentNode == targetNode)
                    {
                        pathSuccess = true;
                        break;
                    }

                    foreach (Node neighbour in request.pathGrid.getNeighbours(currentNode))
                    {
                        if (!neighbour.traversable || closedSet.Contains(neighbour))
                        {
                            continue;
                        }

                        int newMovementCostToNeighbour = currentNode.gCost + getDistance(currentNode, neighbour);
                        if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                        {
                            neighbour.gCost = newMovementCostToNeighbour;
                            neighbour.hCost = getDistance(neighbour, targetNode);
                            neighbour.parent = currentNode;

                            if (!openSet.Contains(neighbour))
                            {
                                openSet.add(neighbour);
                            }
                        }
                    }
                }
            }


            if (pathSuccess)
            {
                wayPoints = retracePath(startNode, targetNode);
            }
            callback(new PathResult(wayPoints, pathSuccess, request.callback));
        }

        Vector2[] retracePath(Node startNode, Node endNode)
        {
            List<Node> path = new List<Node>();
            Node currentNode = endNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode);
                currentNode = currentNode.parent;
            }
            Vector2[] wayPoints = getPathV2(path);
            Array.Reverse(wayPoints);
            return wayPoints;
        }

        Vector2[] simplifyPath(List<Node> path)
        {
            List<Vector2> wayPoints = new List<Vector2>();
            Vector2 directionOld = Vector2.zero;

            for (int i = 1; i < path.Count; i++)
            {
                Vector2 directionNew = new Vector2(path[i - 1].boardPos.x - path[i].boardPos.x, path[i - 1].boardPos.y - path[i].boardPos.y);
                if (directionNew != directionOld)
                {
                    wayPoints.Add(path[i].boardPos);
                }
                directionOld = directionNew;
            }
            return wayPoints.ToArray();
        }

        Vector2[] getPathV2(List<Node> path)
        {
            List<Vector2> wayPoints = new List<Vector2>();
            for (int i = 0; i < path.Count; i++)
            {
                wayPoints.Add(path[i].boardPos);
            }
            return wayPoints.ToArray();
        }

        int getDistance(Node nodeA, Node nodeB)
        {
            int dstX = Mathf.FloorToInt(Mathf.Abs(nodeA.boardPos.x - nodeB.boardPos.x));
            int dstY = Mathf.FloorToInt(Mathf.Abs(nodeA.boardPos.y - nodeB.boardPos.y));

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }
}