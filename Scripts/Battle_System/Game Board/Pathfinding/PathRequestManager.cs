﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
namespace Game.BattleSystem.GameBoard.PathFinding
{
    public class PathRequestManager : MonoBehaviour
    {
        Queue<PathResult> results = new Queue<PathResult>();
        static PathRequestManager instance;
        PathFinding pathFinding;

        void Awake()
        {
            instance = this;
            pathFinding = GetComponent<PathFinding>();
        }

        public static void RequestPath(PathRequest request)
        {
            ThreadStart threadStart = delegate
            {
                instance.pathFinding.findPath(request, instance.finishedProcessingPath);
            };
            threadStart.Invoke();
        }

        void Update()
        {
            if (results.Count > 0)
            {
                int itemsInQueue = results.Count;
                lock (results)
                {
                    for (int i = 0; i < itemsInQueue; i++)
                    {
                        PathResult result = results.Dequeue();
                        result.resultCallback(result.resultPath, result.resultSuccess);
                    }
                }
            }
        }


        public void finishedProcessingPath(PathResult result)
        {
            lock (results)
            {
                results.Enqueue(result);
            }
        }
    }

    public struct PathResult
    {
        public Vector2[] resultPath;
        public bool resultSuccess;
        public Action<Vector2[], bool> resultCallback;

        public PathResult(Vector2[] path, bool success, Action<Vector2[], bool> callback)
        {
            resultPath = path;
            resultSuccess = success;
            resultCallback = callback;
        }
    }


    public struct PathRequest
    {
        public Vector2 pathStart;
        public Vector2 pathEnd;
        public AStarGrid pathGrid;
        public Action<Vector2[], bool> callback;

        public PathRequest(Vector2 start, Vector2 end, AStarGrid grid, Action<Vector2[], bool> callBack)
        {
            pathStart = start;
            pathEnd = end;
            pathGrid = grid;
            callback = callBack;

        }
    }
}