﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.GameBoard.PathFinding
{
    public class Node : IHeapItem<Node>
    {
        //walkable
        public bool traversable;
        public Vector3 worldPosition;
        //grid x grid y
        public Vector2 boardPos;

        public int gCost;
        public int hCost;
        public Node parent;
        int heapIndex;

        public Node(bool traversable, Vector3 worldPosition, Vector2 boardPos)
        {
            this.traversable = traversable;
            this.worldPosition = worldPosition;
            this.boardPos = boardPos;
        }

        public int fCost { get { return gCost + hCost; } }
        public int HeapIndex
        {
            get { return heapIndex; }
            set { heapIndex = value; }
        }

        public int CompareTo(Node nodeToCompare)
        {
            int compare = fCost.CompareTo(nodeToCompare.fCost);
            if (compare == 0)
            {
                compare = hCost.CompareTo(nodeToCompare.hCost);
            }
            return -compare;
        }
    }
}