﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Events;
using Game.BattleSystem.Units.BoardUnits;
namespace Game.BattleSystem.GameBoard.PathFinding
{
    public class AStarGrid
    {
        private Dictionary<string, Node> grid;
        private Board board;
        int gridSizeX;
        int gridSizeY;

        public int MaxSize
        {
            get { return gridSizeX * gridSizeY; }
        }

        public void createGrid(Board board, int range, AbstractBoardUnit unit)
        {
            this.board = board;
            grid = new Dictionary<string, Node>();
            gridSizeX = 2 * range;
            gridSizeY = 2 * range;

            Vector2 unitOrigin = unit.origin();
            for (int i = -range; i < range; i++)
            {
                for (int j = -range; j < range; j++)
                {
                    bool isTraversable = true;
                    Vector2 v2 = unitOrigin + new Vector2(i, j);
                    if (board.coordIsInBounds(v2))
                    {
                        MovementEventInfo potentialMovementInfo = new MovementEventInfo(board, unit, range);


                        if (unit.IsAirUnit)
                        {
                            isTraversable = potentialMovementInfo.isAirSpaceAvailable(v2);
                        }
                        else
                        {
                            isTraversable = potentialMovementInfo.isGroundSpaceAvailable(v2);
                        }
                        grid.Add(v2.ToString(), new Node(isTraversable, board.getGlobalPos(v2), v2));
                    }

                }
            }
        }

        public List<Node> getNeighbours(Node node)
        {
            List<Node> neighbours = new List<Node>();
            Vector2[] vectors = new Vector2[] { new Vector2(0, 1), new Vector2(0, -1), new Vector2(1, 0), new Vector2(-1, 0) };
            foreach (Vector2 v2 in vectors)
            {
                Vector2 check = node.boardPos + v2;
                if (this.grid.ContainsKey(check.ToString()))
                {
                    neighbours.Add(grid[check.ToString()]);
                }
            }

            return neighbours;
        }

        public Node nodeFromWorldPoint(Vector3 worldPosition)
        {
            Vector2 v2 = board.getGridPosition(worldPosition);
            return grid[v2.ToString()];
        }

        public Node nodeFromGridPosition(Vector2 gridPosition)
        {
            return grid[gridPosition.ToString()];
        }

        public List<Node> path;
    }
}