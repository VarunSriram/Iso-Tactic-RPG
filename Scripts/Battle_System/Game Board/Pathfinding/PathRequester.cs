﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Utility;

namespace Game.BattleSystem.GameBoard.PathFinding
{
    public class PathRequester : MonoBehaviour
    {
        public Vector2[] path;
        public bool pathCalcComplete = false;
        public bool pathFoundFlag = false;

        public void onPathFound(Vector2[] newPath, bool pathSuccessful)
        {
            if (pathSuccessful)
            {
                DebugLogger.instance.logInfo("Request from Request " + this.GetHashCode() + " successful!", GetHashCode(), GetType(), gameObject.name);
                path = newPath;
                StopCoroutine("flagPathIsMade");
                StartCoroutine("flagPathIsMade");
                pathFoundFlag = true;
            }
            else
            {
                pathFoundFlag = false;
                DebugLogger.instance.logInfo("Request from Request " + this.GetHashCode() + " unsuccessful!", GetHashCode(), GetType(), gameObject.name);
            }
            pathCalcComplete = true;
        }

        public void requestPath(Vector2 start, Vector2 target, AStarGrid grid)
        {
            path = new Vector2[0];
            pathCalcComplete = false;
            PathRequestManager.RequestPath(new PathRequest(start, target, grid, onPathFound));

        }

        IEnumerator flagPathIsMade()
        {
            while (true)
            {
                if (this.path == null || this.path.Length == 0)
                {

                    yield return null;
                }
                printPath();
                yield break;
            }
        }

        public void printPath()
        {
            string message = "The requested path from requester " + this.GetHashCode() + ": ";
            foreach (Vector2 v2 in path)
            {
                message += v2.ToString() + "| ";
            }

            DebugLogger.instance.logInfo(message, GetHashCode(), GetType(), gameObject.name);
        }


    }
}