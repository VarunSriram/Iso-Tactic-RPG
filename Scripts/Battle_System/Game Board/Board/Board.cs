﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.GameBoard
{
    public class Board : MonoBehaviour
    {
        [SerializeField]
        private Dictionary<string, Tile> tileManifest = new Dictionary<string, Tile>();
        [SerializeField]
        private Dictionary<string, Vector2> positionMap = new Dictionary<string, Vector2>();
        [SerializeField]
        private int width;
        [SerializeField]
        private int height;

        public void registerTile(Vector2 gridPos, Tile t)
        {
            tileManifest.Add(gridPos.ToString(), t);
            positionMap.Add(t.getPosition().ToString(), gridPos);
        }

        public bool sanityCheckBoard()
        {
            return tileManifest.Keys.Count == width * height;
        }

        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public Vector3 getGlobalPos(Vector2 gridPos)
        {
            return tileManifest[gridPos.ToString()].getPosition();
        }

        public Vector2 getGridPosition(Vector3 position)
        {
            return positionMap[position.ToString()];
        }

        public bool coordIsInBounds(Vector2 gridPos)
        {
            return this.tileManifest.ContainsKey(gridPos.ToString());
        }

        public bool isTileWalkable(Vector2 tileCoord)
        {
            bool isInBounds = this.coordIsInBounds(tileCoord);
            if (isInBounds)
            {
                return this.tileManifest[tileCoord.ToString()].Walkable;
            }
            else
            {
                return false;
            }
        }

        public bool isTileGroundOccupied(Vector2 tileCoord)
        {
            bool isInBounds = this.coordIsInBounds(tileCoord);
            if (isInBounds)
            {
                return this.tileManifest[tileCoord.ToString()].GroundSpaceOccupied;
            }
            else
            {
                return true;
            }
        }

        public bool isTileAirOccupied(Vector2 tileCoord)
        {
            bool isInBounds = this.coordIsInBounds(tileCoord);
            if (isInBounds)
            {
                return this.tileManifest[tileCoord.ToString()].AirSpaceOccupied;
            }
            else
            {
                return true;
            }
        }

        public bool isGroundTileSpaceOccupied(List<Vector2> vectors)
        {
            foreach (Vector2 v2 in vectors)
            {
                if (isTileGroundOccupied(v2) || !isTileWalkable(v2))
                {
                    return true;
                }
            }
            return false;
        }



        public bool isAirTileSpaceOccupied(List<Vector2> vectors)
        {
            foreach (Vector2 v2 in vectors)
            {
                if (isTileAirOccupied(v2))
                {
                    return true;
                }
            }
            return false;
        }

        public void releaseTileGroundSpace(List<Vector2> vectors)
        {
            this.setGroundSpaceOccupied(vectors, false);
        }

        public void releaseTileAirSpace(List<Vector2> vectors)
        {
            this.setAirSpaceOccupied(vectors, false);
        }

        public void occupyTileGroundSpace(List<Vector2> vectors)
        {
            this.setGroundSpaceOccupied(vectors, true);
        }

        public void occupyAirSpace(List<Vector2> vectors)
        {
            this.setAirSpaceOccupied(vectors, true);
        }

        public bool transferGroundOccupation(List<Vector2> source, List<Vector2> destination)
        {
            if (source.Count != destination.Count)
            {
                Debug.LogError("Cannot transfer occupation as number of vectors in source does not match the count in the destination!");
                return false;
            }
            else
            {
                setGroundSpaceOccupied(source, false);
                setGroundSpaceOccupied(destination, true);
                return true;
            }
        }

        public bool transferAirOccupation(List<Vector2> source, List<Vector2> destination)
        {
            if (source.Count != destination.Count)
            {
                Debug.LogError("Cannot transfer occupation as number of vectors in source does not match the count in the destination!");
                return false;
            }
            else
            {
                setAirSpaceOccupied(source, false);
                setAirSpaceOccupied(destination, true);
                return true;
            }
        }

        private void setAirSpaceOccupied(List<Vector2> vectors, bool setting)
        {
            foreach (Vector2 v2 in vectors)
            {
                setAirSpaceOccupied(v2, setting);
            }
        }



        private void setGroundSpaceOccupied(List<Vector2> vectors, bool setting)
        {
            foreach (Vector2 v2 in vectors)
            {
                setGroundSpaceOccupied(v2, setting);
            }
        }

        private void setGroundSpaceOccupied(Vector2 vector, bool setting)
        {
            Tile t;
            tileManifest.TryGetValue(vector.ToString(), out t);
            if (t != null)
            {
                t.GroundSpaceOccupied = setting;
            }
        }

        private void setAirSpaceOccupied(Vector2 vector, bool setting)
        {
            Tile t;
            tileManifest.TryGetValue(vector.ToString(), out t);
            if (t != null)
            {
                t.AirSpaceOccupied = setting;
            }
        }


    }
}
