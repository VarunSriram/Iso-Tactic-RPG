﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField]
    private string playerName;
	

    public string PlayerName
    {
        get { return this.playerName; }
        set { playerName = value; }
    }
}
