﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.BattleSystem.GameBoard
{
    public class Tile : MonoBehaviour
    {
        Dictionary<string, GameObject> passivesManifest;
        Dictionary<string, GameObject> tileStatusManifest;
        [SerializeField]
        private bool isWalkable;
        [SerializeField]
        private bool isGroundSpaceOccupied = false;
        [SerializeField]
        private bool isAirSpaceOccupied = false;
        // Use this for initialization
        [SerializeField]
        private Transform pEffects;
        [SerializeField]
        private Transform tStatus;
        [SerializeField]
        private Vector2 boardCoord;
        [SerializeField]
        private bool debugShadows = true;
        void Awake()
        {
            MeshRenderer mr = GetComponent<MeshRenderer>();
            if (mr != null)
            {
                mr.receiveShadows = debugShadows;
            }
            this.loadPassiveManifest();
            this.loadStatusManifest();
        }

        public bool isPassiveEnabled(string name)
        {
            return passivesManifest[name].activeSelf;
        }

        public void enablePassive(string name)
        {
            passivesManifest[name].SetActive(true);
        }

        public void disablePassive(string name)
        {
            passivesManifest[name].SetActive(false);
        }

        public void viewTileStatus(string name)
        {
            tileStatusManifest[name].SetActive(true);
        }

        public void hideTileStatus(string name)
        {
            tileStatusManifest[name].SetActive(false);
        }

        public void hideAllTileStatus()
        {
            foreach (var item in tileStatusManifest)
            {
                item.Value.SetActive(false);
            }
        }


        public Vector3 getPosition()
        {
            return transform.position;
        }

        public bool Walkable
        {
            get { return isWalkable; }
            set { isWalkable = value; }
        }

        public bool GroundSpaceOccupied
        {
            get { return isGroundSpaceOccupied; }
            set { isGroundSpaceOccupied = value; }
        }

        public bool AirSpaceOccupied
        {
            get { return isAirSpaceOccupied; }
            set { isAirSpaceOccupied = value; }
        }

        public Vector2 BoardCoord
        {
            get { return boardCoord; }
            set { boardCoord = value; }
        }

        private void loadPassiveManifest()
        {
            passivesManifest = new Dictionary<string, GameObject>();
            if (pEffects.childCount > 0)
            {
                foreach (Transform t in pEffects)
                {
                    passivesManifest.Add(t.name, t.gameObject);
                    t.gameObject.SetActive(false);
                }
            }
        }

        private void loadStatusManifest()
        {
            tileStatusManifest = new Dictionary<string, GameObject>();
            if (tStatus.childCount > 0)
            {
                foreach (Transform t in tStatus)
                {
                    tileStatusManifest.Add(t.name, t.gameObject);
                    t.gameObject.isStatic = true;
                    t.gameObject.SetActive(false);
                }
            }
        }
    }
}