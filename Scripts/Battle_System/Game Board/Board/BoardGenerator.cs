﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Events;
using Game.BattleSystem.GameBoard.Serialization;
using Game.BattleSystem.GameBoard.Serialization.Factories;
using Game.BattleSystem.GameBoard.Serialization.Domain;
using Game.BattleSystem.GameBoard.Util;
using Game.Utility;

namespace Game.BattleSystem.GameBoard
{
    public class BoardGenerator : MonoBehaviour
    {
        private BoardModelFactory boardModelFactory;
        private BoardDescriptorLoader boardDescriptorLoader;
        private Dictionary<string, GameObject> tileManifest;
        private Dictionary<string, string> tileSymbolManifest;
        private List<string> hillSymbolList;
        [SerializeField]
        private string debugDescriptorName;
        [SerializeField]
        private bool debugMode;
        [SerializeField]
        private GameObject refObj;

        [SerializeField]
        private Board boardComponent;

        void Awake()
        {
            boardModelFactory = new BoardModelFactory();
            boardDescriptorLoader = GetComponent<BoardDescriptorLoader>();
            boardComponent = GetComponent<Board>();
            this.populateSymbolManifest();
            this.populateTileManifest();
        }

        void Start()
        {
            if (debugMode)
            {
                generateBoard("");
            }
        }

        public void generateBoard(string descriptorName)
        {
            if (debugMode)
            {
                DebugLogger.instance.logInfo("Generating " + debugDescriptorName + ".json", GetHashCode(), GetType(), gameObject.name);
                BoardModel boardModel = (BoardModel)boardModelFactory.createInstance(boardDescriptorLoader.getDescriptorFileText(debugDescriptorName));
                constructBoardInScene(boardModel);
            }
            else
            {
                BoardModel boardModel = (BoardModel)boardModelFactory.createInstance(boardDescriptorLoader.getDescriptorFileText(descriptorName));
                constructBoardInScene(boardModel);
            }
        }

        private void constructBoardInScene(BoardModel boardModel)
        {
            boardComponent.Width = boardModel.width;
            boardComponent.Height = boardModel.height;
            transform.rotation = Quaternion.Euler(-90f, 0f, -45f);

            for (int y = 0; y < boardModel.width; y++)
            {
                GameObject colRef = Instantiate(refObj);
                colRef.name = ("col: " + y);
                colRef.transform.parent = transform;
                colRef.transform.localPosition = new Vector3(-y, 0f, 0f);
                colRef.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                for (int x = 0; x < boardModel.height; x++)
                {
                    string symbol = boardModel.tileData.tiles[boardModel.width * y + x];
                    GameObject go = Instantiate(tileManifest[symbol]);

                    go.transform.parent = colRef.transform;
                    go.transform.localPosition = new Vector3(0, x, 0f);
                    go.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                    Tile tile = go.GetComponent<Tile>();
                    if (!(symbol.Equals(TileConstants.squareSymbol) || TileConstants.isHillSymbol(symbol)))
                        tile.Walkable = false;
                    else
                        tile.Walkable = true;

                    growGrass(boardModel, tile, x, y);
                    tile.BoardCoord = new Vector2(y, x);
                    boardComponent.registerTile(new Vector2(y, x), tile);
                    TileStatusViewerController tsvc = go.GetComponent<TileStatusViewerController>();
                    tsvc.initializeEvents();
                }
            }
        }

        private void growGrass(BoardModel bm, Tile t, int posX, int posY)
        {
            if (bm.tileData.grassMap[bm.width * posY + posX].Equals(TileConstants.grassSymbol))
            {
                t.enablePassive(TileConstants.tallGrassPassiveEffect);
            }
        }





        private void populateTileManifest()
        {
            tileManifest = new Dictionary<string, GameObject>();
            GameObject[] tilePrefabs = Resources.LoadAll<GameObject>("Tiles/Prefabs");
            foreach (GameObject tile in tilePrefabs)
            {
                if (!tileSymbolManifest.ContainsKey(tile.name))
                {
                    Debug.LogError("The prefab " + tile.name + " has no symbol mapped to it!");
                }
                else
                {
                    tileManifest.Add(tileSymbolManifest[tile.name], tile);
                }
            }
        }

        private void populateSymbolManifest()
        {
            tileSymbolManifest = new Dictionary<string, string>();
            tileSymbolManifest.Add(TileConstants.squareTileName, TileConstants.squareSymbol);
            tileSymbolManifest.Add(TileConstants.northSideTileName, TileConstants.northSideSymbol);
            tileSymbolManifest.Add(TileConstants.southSideTileName, TileConstants.southSideSymbol);
            tileSymbolManifest.Add(TileConstants.westSideTileName, TileConstants.westSideSymbol);
            tileSymbolManifest.Add(TileConstants.eastSideTileName, TileConstants.eastSideSymbol);

            tileSymbolManifest.Add(TileConstants.northWestCurveTileName, TileConstants.northWestCurveSymbol);
            tileSymbolManifest.Add(TileConstants.northEastCurveTileName, TileConstants.northEastCurveSymbol);
            tileSymbolManifest.Add(TileConstants.southWestCurveTileName, TileConstants.southWestCurveSymbol);
            tileSymbolManifest.Add(TileConstants.southEastCurveTileName, TileConstants.southEastCurveSymbol);

            tileSymbolManifest.Add(TileConstants.northWestCornerTileName, TileConstants.northWestCornerSymbol);
            tileSymbolManifest.Add(TileConstants.northEastCornerTileName, TileConstants.northEastCornerSymbol);
            tileSymbolManifest.Add(TileConstants.southWestCornerTileName, TileConstants.southWestCornerSymbol);
            tileSymbolManifest.Add(TileConstants.southEastCornerTileName, TileConstants.southEastCornerSymbol);

            tileSymbolManifest.Add(TileConstants.hillSquareTileName, TileConstants.hillSquareSymbol);
            tileSymbolManifest.Add(TileConstants.hillNorthSideTileName, TileConstants.hillNorthSideSymbol);
            tileSymbolManifest.Add(TileConstants.hillSouthSideTileName, TileConstants.hillSouthSideSymbol);
            tileSymbolManifest.Add(TileConstants.hillWestSideTileName, TileConstants.hillWestSideSymbol);
            tileSymbolManifest.Add(TileConstants.hillEastSideTileName, TileConstants.hillEastSideSymbol);

            tileSymbolManifest.Add(TileConstants.hillNorthWestCurveTileName, TileConstants.hillNorthWestCurveSymbol);
            tileSymbolManifest.Add(TileConstants.hillNorthEastCurveTileName, TileConstants.hillNorthEastCurveSymbol);
            tileSymbolManifest.Add(TileConstants.hillSouthWestCurveTileName, TileConstants.hillSouthWestCurveSymbol);
            tileSymbolManifest.Add(TileConstants.hillSouthEastCurveTileName, TileConstants.hillSouthEastCurveSymbol);

            tileSymbolManifest.Add(TileConstants.hillNorthWestCornerTileName, TileConstants.hillNorthWestCornerSymbol);
            tileSymbolManifest.Add(TileConstants.hillNorthEastCornerTileName, TileConstants.hillNorthEastCornerSymbol);
            tileSymbolManifest.Add(TileConstants.hillSouthWestCornerTileName, TileConstants.hillSouthWestCornerSymbol);
            tileSymbolManifest.Add(TileConstants.hillSouthEastCornerTileName, TileConstants.hillSouthEastCornerSymbol);


            tileSymbolManifest.Add(TileConstants.emptyTileName, TileConstants.emptySymbol);

        }
    }
}
