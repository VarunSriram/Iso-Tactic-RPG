﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Util;
using Game.Events;
namespace Game.BattleSystem.GameBoard.Events
{
    public class HideAirTileStatusEvent : GameEvent
    {
        [SerializeField]
        private Tile tile;

        public void Awake()
        {
            tile = GetComponent<Tile>();
            this.eventName = BoardEventConstants.hideAirSpace;
        }

        protected override void eventAction()
        {
            this.hideTileStatus();
        }

        private void hideTileStatus()
        {
            tile.hideTileStatus(TileConstants.flyableStatus);
            tile.hideTileStatus(TileConstants.nonFlyableStatus);
        }
    }
}