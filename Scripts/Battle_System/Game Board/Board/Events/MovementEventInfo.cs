﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Game.BattleSystem.GameBoard.PathFinding;
using Game.BattleSystem.Units.BoardUnits;
namespace Game.BattleSystem.GameBoard.Events
{
    public class MovementEventInfo
    {
        private Board board;
        private AbstractBoardUnit unit;
        private int range;
        private AStarGrid aStarGrid;
        //Constructor with  movement range of 1;
        public MovementEventInfo(Board board, AbstractBoardUnit unit)
        {
            this.board = board;
            this.unit = unit;
            this.range = 1;
        }
        //Constructor with a given range of movement.
        public MovementEventInfo(Board board, AbstractBoardUnit unit, int range)
        {
            this.board = board;
            this.unit = unit;

            this.range = range;
        }

        public Board GameBoard
        {
            get { return this.board; }
            set { this.board = value; }
        }

        public AbstractBoardUnit Unit
        {
            get { return this.unit; }
            set { this.unit = value; }
        }

        public int Range
        {
            get { return this.range; }
            set { this.range = value; }
        }

        public AStarGrid AStarGrid
        {
            get { return this.aStarGrid; }
            set { this.aStarGrid = value; }
        }

        public bool isGroundSpaceAvailable(Vector2 v2)
        {
            List<Vector2> currentSpace = unit.getBoardSpace();
            List<Vector2> potentialSpace = unit.getBoardSpace(v2);
            List<Vector2> intersection = new List<Vector2>(currentSpace.Intersect(potentialSpace));
            potentialSpace = new List<Vector2>(potentialSpace.Except(intersection));
            return !board.isGroundTileSpaceOccupied(potentialSpace);
        }

        public bool isAirSpaceAvailable(Vector2 v2)
        {
            List<Vector2> currentSpace = unit.getBoardSpace();
            List<Vector2> potentialSpace = unit.getBoardSpace(v2);
            List<Vector2> intersection = new List<Vector2>(currentSpace.Intersect(potentialSpace));
            potentialSpace = new List<Vector2>(potentialSpace.Except(intersection));
            return !board.isAirTileSpaceOccupied(potentialSpace);
        }

        public bool isVectorInRange(Vector2 v2)
        {
            int dist = manhattanDistance(v2, unit.origin());
            return dist > 0 && dist <= range;
        }

        private int manhattanDistance(Vector2 a, Vector2 b)
        {
            return Mathf.FloorToInt(Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y));
        }
    }
}