﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard;
using Game.BattleSystem.GameBoard.PathFinding;
using Game.BattleSystem.GameBoard.Util;
using Game.BattleSystem.Units.BoardUnits;
using Game.Events;
namespace Game.BattleSystem.GameBoard.Events
{
    public class TileStatusTrigger : MonoBehaviour, EventTrigger
    {
        [SerializeField]
        private Boolean groundViewOn = false;
        [SerializeField]
        private Boolean airViewOn = false;
        [SerializeField]
        private Boolean gMovementOn = false;
        [SerializeField]
        private Boolean aMovementOn = false;
        [SerializeField]
        private Board boardTest;
        [SerializeField]
        private AbstractBoardUnit testUnit;
        [SerializeField]
        PathRequester pathRequester;
        public int testRange = 3;

        public void triggerEvent()
        {
            if (Input.GetKeyDown("q"))
            {

                if (!groundViewOn)
                {
                    EventManager.TriggerEvent(BoardEventConstants.showGroundSpace);
                    groundViewOn = true;
                }
                else
                {
                    EventManager.TriggerEvent(BoardEventConstants.hideGroundSpace);
                    groundViewOn = false;
                }
            }

            if (Input.GetKeyDown("e"))
            {
                if (!airViewOn)
                {
                    EventManager.TriggerEvent(BoardEventConstants.showAirSpace);
                    airViewOn = true;
                }
                else
                {
                    EventManager.TriggerEvent(BoardEventConstants.hideAirSpace);
                    airViewOn = false;
                }
            }

            if (Input.GetKeyDown("r"))
            {
                if (!gMovementOn)
                {
                    testUnit.moveUnit(new Vector2(12, 10));
                    boardTest.occupyTileGroundSpace(testUnit.getBoardSpace());
                    boardTest.occupyAirSpace(testUnit.getBoardSpace());
                    AStarGrid aStarGrid = new AStarGrid();
                    aStarGrid.createGrid(boardTest, testRange + 1, testUnit);
                    MovementEventInfo info = new MovementEventInfo(boardTest, testUnit, testRange);
                    info.AStarGrid = aStarGrid;
                    EventManager.TriggerEvent(BoardEventConstants.showGroundMovementSpace, info);
                    gMovementOn = true;
                }
                else
                {
                    EventManager.TriggerEvent(BoardEventConstants.hideGroundMovementSpace);
                    gMovementOn = false;
                }
            }

            if (Input.GetKeyDown("f"))
            {
                if (!aMovementOn)
                {
                    boardTest.occupyTileGroundSpace(testUnit.getBoardSpace());
                    boardTest.occupyAirSpace(testUnit.getBoardSpace());
                    AStarGrid aStarGrid = new AStarGrid();
                    aStarGrid.createGrid(boardTest, testRange + 1, testUnit);
                    MovementEventInfo info = new MovementEventInfo(boardTest, testUnit, testRange);
                    info.AStarGrid = aStarGrid;
                    EventManager.TriggerEvent(BoardEventConstants.showAirMovementSpace, info);
                    aMovementOn = true;
                }
                else
                {
                    EventManager.TriggerEvent(BoardEventConstants.hideAirMovementSpace);
                    aMovementOn = false;
                }
            }

            if (Input.GetKeyDown("p"))
            {
                EventManager.instance.printAllEvents();
            }

            if (Input.GetKeyDown("b"))
            {
                testUnit.moveUnit(new Vector2(12, 10));


                AStarGrid unitMovementGrid = new AStarGrid();
                unitMovementGrid.createGrid(boardTest, testRange, testUnit);
                pathRequester.requestPath(new Vector2(12, 10), new Vector2(14, 11), unitMovementGrid);
            }
            if (Input.GetKeyDown("v"))
            {
                pathRequester.printPath();
            }
        }

        void Update()
        {
            triggerEvent();
        }
    }
}