﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Util;
using Game.Events;
using Game.Utility;

namespace Game.BattleSystem.GameBoard.Events
{
    public class ViewAirTileStatusEvent : GameEvent
    {
        [SerializeField]
        private Tile tile;

        public void Awake()
        {
            tile = GetComponent<Tile>();
            this.eventName = BoardEventConstants.showAirSpace;
        }

        protected override void eventAction()
        {
            this.showAirSpaceAvailability();
        }

        private void showAirSpaceAvailability()
        {

            DebugLogger.instance.logInfo("Showing Air Space", GetHashCode(), GetType(), gameObject.name);
            if (!tile.AirSpaceOccupied)
            {
                tile.viewTileStatus(TileConstants.flyableStatus);
            }
            else
            {
                tile.viewTileStatus(TileConstants.nonFlyableStatus);
            }

        }

    }
}