﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.PathFinding;
using Game.BattleSystem.GameBoard.Util;
using Game.Events;
using Game.Utility;

namespace Game.BattleSystem.GameBoard.Events
{
    public class ViewAirMovementStatusEvent : GameEventOneArg
    {
        [SerializeField]
        private Tile tile;
        [SerializeField]
        private PathRequester pathRequester;
        public void Awake()
        {
            tile = GetComponent<Tile>();
            pathRequester = GetComponent<PathRequester>();
            this.eventName = BoardEventConstants.showAirMovementSpace;
        }

        protected override void eventAction(object arg0)
        {
            MovementEventInfo info = (MovementEventInfo)arg0;
            this.showMovementSpace(info);
        }

        private void showMovementSpace(MovementEventInfo info)
        {
            if (!info.isVectorInRange(tile.BoardCoord))
            {
                return;
            }

            pathRequester.requestPath(info.Unit.origin(), tile.BoardCoord, info.AStarGrid);
            StopCoroutine("waitForPathFound");
            StartCoroutine("waitForPathFound");

        }

        IEnumerator waitForPathFound()
        {
            while (true)
            {
                if (!pathRequester.pathCalcComplete)
                {
                    yield return null;
                }
                else
                {
                    if (pathRequester.pathFoundFlag == true)
                    {
                        DebugLogger.instance.logInfo("Showing Ground Movement status", GetHashCode(), GetType(), gameObject.name);
                        tile.viewTileStatus(TileConstants.airMovement);
                    }
                    else
                    {
                        DebugLogger.instance.logInfo("Hiding Ground Movement status", GetHashCode(), GetType(), gameObject.name);
                        tile.hideTileStatus(TileConstants.airMovement);
                    }
                    yield break;
                }
            }
        }
    }
}