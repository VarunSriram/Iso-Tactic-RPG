﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Util;
using Game.Events;
using Game.Utility;
namespace Game.BattleSystem.GameBoard.Events
{
    public class ViewTileStatusEvent : GameEvent
    {
        [SerializeField]
        private Tile tile;

        public void Awake()
        {
            tile = GetComponent<Tile>();
            this.eventName = BoardEventConstants.showGroundSpace;
        }

        protected override void eventAction()
        {
            this.showGroundSpaceAvailability();
        }

        private void showGroundSpaceAvailability()
        {
            DebugLogger.instance.logInfo("Showing Ground Space", GetHashCode(), GetType(), gameObject.name);
            if (tile.Walkable && !tile.GroundSpaceOccupied)
            {
                tile.viewTileStatus(TileConstants.walkableStatus);
            }
            else
            {
                tile.viewTileStatus(TileConstants.nonWalkableStatus);
            }
        }
    }
}