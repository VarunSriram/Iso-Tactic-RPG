﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.Units.BoardUnits;
using Game.Utility;
namespace Game.BattleSystem.GameBoard
{
    public class BoardMaster : MonoBehaviour
    {
        [SerializeField]
        private Board gameBoard;
        private Dictionary<Player, Dictionary<string, AbstractBoardUnit>> playerManifest;
        void Awake()
        {
            gameBoard = GetComponent<Board>();
            playerManifest = new Dictionary<Player, Dictionary<string, AbstractBoardUnit>>();
        }

        public Board GameBoard
        {
            get { return gameBoard; }
        }

        public void registerPlayer(Player player)
        {
            playerManifest.Add(player, new Dictionary<string, AbstractBoardUnit>());
        }

        public void registerBoardUnit(Player player, AbstractBoardUnit boardUnit)
        {
            Dictionary<string, AbstractBoardUnit> unitManifest = null;
            playerManifest.TryGetValue(player, out unitManifest);
            if (unitManifest != null)
            {
                unitManifest.Add(boardUnit.UnitName, boardUnit);
            }
        }

        public void removeBoardUnit(Player player, AbstractBoardUnit boardUnit)
        {
            Dictionary<string, AbstractBoardUnit> unitManifest = null;
            playerManifest.TryGetValue(player, out unitManifest);
            if (unitManifest != null)
            {
                unitManifest.Remove(boardUnit.UnitName);
            }
        }

        public bool moveBoardUnit(Player player, AbstractBoardUnit unit, Vector2 destination)
        {
            Dictionary<string, AbstractBoardUnit> unitManifest = null;
            playerManifest.TryGetValue(player, out unitManifest);
            if (unitManifest != null)
            {
                AbstractBoardUnit boardUnit;
                unitManifest.TryGetValue(unit.UnitName, out boardUnit);
                if (boardUnit != null)
                {
                    if (boardUnit.IsAirUnit)
                    {
                        return this.moveAirUnit(boardUnit, destination);
                    }
                    else
                    {
                        return this.moveGroundUnit(boardUnit, destination);
                    }
                }
                else
                {
                    DebugLogger.instance.logError("Unit " + unit.UnitName + "is not registered for player " + player.PlayerName, GetHashCode(), GetType(), gameObject.name);
                    return false;
                }
            }
            else
            {
                DebugLogger.instance.logError("Unit Movement failed!: Player " + player.PlayerName + " is not registered!", GetHashCode(), GetType(), gameObject.name);
                return false;
            }
        }
        private bool moveAirUnit(AbstractBoardUnit boardUnit, Vector2 destination)
        {
            List<Vector2> theoreticalOccupyingSpace = boardUnit.getBoardSpace(destination);
            if (gameBoard.isAirTileSpaceOccupied(theoreticalOccupyingSpace))
            {
                DebugLogger.instance.logInfo("Cannot Move " + boardUnit.UnitName + " as something is occupying that space", GetHashCode(), GetType(), gameObject.name);
                return false;
            }
            else
            {
                bool moveSuccess = gameBoard.transferAirOccupation(boardUnit.getBoardSpace(), theoreticalOccupyingSpace);
                if (moveSuccess)
                {
                    boardUnit.moveUnit(destination);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool moveGroundUnit(AbstractBoardUnit boardUnit, Vector2 destination)
        {
            List<Vector2> theoreticalOccupyingSpace = boardUnit.getBoardSpace(destination);
            if (gameBoard.isGroundTileSpaceOccupied(theoreticalOccupyingSpace))
            {
                return false;
            }
            else
            {

                bool moveSuccess = gameBoard.transferGroundOccupation(boardUnit.getBoardSpace(), theoreticalOccupyingSpace);
                if (moveSuccess)
                {
                    boardUnit.moveUnit(destination);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
