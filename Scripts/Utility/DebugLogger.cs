﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Utility
{
    public class DebugLogger : MonoBehaviour
    {
        public static DebugLogger instance;
        [SerializeField]
        private bool debugEnabled = true;

        void Awake()
        {
            if (instance == null)
            {
                //if not, set instance to this
                instance = this;
            }

            //If instance already exists and it's not this:
            else if (instance != this)
            {
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);
            }
            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }

        public void logInfo(string message, int hashCode, System.Type className, string gameObjName = "")
        {
            if (!debugEnabled)
            {
                return;
            }
            string log = "INFO:-" + hashCode + " | ";
            if (!string.IsNullOrEmpty(gameObjName))
            {
                log += "GameObject: " + gameObjName + " | ";
            }
            log += className.Name + " | ";
            log += message;
            Debug.Log(log);
        }

        public void logError(string message, int hashCode, System.Type classType, string gameObjName = "")
        {
            if (!debugEnabled)
            {
                return;
            }
            string log = "ERROR:-" + hashCode + " | ";
            if (!string.IsNullOrEmpty(gameObjName))
            {
                log += "GameObject: " + gameObjName + " | ";
            }
            log += classType.Name + " | ";
            log += message;
            Debug.LogError(log);
        }


        public bool DebugEnabled
        {
            get { return debugEnabled; }
            set { debugEnabled = value; }
        }
    }
}