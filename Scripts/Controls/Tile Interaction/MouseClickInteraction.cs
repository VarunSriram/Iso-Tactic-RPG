﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Controls.TileInteractions
{

    public abstract class MouseClickInteraction : MonoBehaviour
    {

        public abstract void clickAction();
        void OnMouseDown()
        {
            this.clickAction();
        }
    }
}