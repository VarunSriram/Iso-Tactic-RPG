﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard;
using Game.Utility;

namespace Game.Controls.TileInteractions
{

    public class MovementSelect : MouseClickInteraction
    {
        [SerializeField]
        private Tile tile;

        void Awake()
        {
            GetComponent<Tile>();
        }
        public override void clickAction()
        {
            DebugLogger.instance.logInfo(tile.BoardCoord.ToString(), GetHashCode(), GetType(), gameObject.name);
        }
    }
}