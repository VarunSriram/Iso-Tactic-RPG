﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Controls.TileInteractions
{

    public abstract class MouseHoverInteraction : MonoBehaviour
    {

        public abstract void hoverAction();
        void OnMouseOver()
        {
            this.hoverAction();
        }


    }
}