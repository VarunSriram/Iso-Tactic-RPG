﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Controls
{
    public class CameraMovement : MonoBehaviour
    {
        public float speed = 5.0f;
        public float minZoom = 2.5f;
        public float maxZoom = 7.5f;
        void Update()
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position += new Vector3(0, -speed * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += new Vector3(0, speed * Time.deltaTime, 0);
            }

            if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
            {
                Camera.main.orthographicSize++;
                if (Camera.main.orthographicSize >= maxZoom)
                {
                    Camera.main.orthographicSize = maxZoom;
                }
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
            {
                Camera.main.orthographicSize--;
                if (Camera.main.orthographicSize <= minZoom)
                {
                    Camera.main.orthographicSize = minZoom;
                }

            }
        }
    }
}