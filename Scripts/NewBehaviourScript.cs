﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.BattleSystem.GameBoard.Serialization.Domain;
public class NewBehaviourScript : MonoBehaviour {

    // Use this for initialization
    public string testString = "{\"boardName\" : \"Test Board\", \"width\" : 11, \"height\" : 11, \"tiles\" : [ \"X\",\"X\",\"X\",\"@\",\"7\",\"#\",\"X\",\"X\",\"@\",\"7\",\"#\", \"X\",\"@\",\"7\",\"1\",\"0\",\"2\",\"7\",\"7\",\"1\",\"0\",\"6\", \"X\",\"5\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"6\", \"X\",\"%\",\"3\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"6\", \"X\",\"X\",\"5\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"4\",\"$\", \"X\",\"@\",\"1\",\"0\",\"0\",\"0\",\"0\",\"0\",\"4\",\"$\",\"X\", \"X\",\"5\",\"0\",\"0\",\"0\",\"0\",\"0\",\"4\",\"$\",\"X\",\"X\", \"X\",\"5\",\"0\",\"0\",\"0\",\"0\",\"0\",\"2\",\"7\",\"#\",\"X\", \"X\",\"5\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"2\",\"#\", \"X\",\"%\",\"3\",\"0\",\"4\",\"3\",\"0\",\"0\",\"0\",\"0\",\"6\", \"X\",\"X\",\"%\",\"8\",\"$\",\"%\",\"8\",\"8\",\"8\",\"8\",\"$\"]}";

    void Start () {
        string text = System.IO.File.ReadAllText(Application.dataPath + "/Scripts/TestBoard.json");
        Debug.Log(text);
        BoardModel board = JsonUtility.FromJson<BoardModel>(text);
        foreach (string s in board.tileData.tiles)
            Debug.Log(s);
	}
	
}
