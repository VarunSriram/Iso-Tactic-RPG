﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using Game.Utility;

namespace Game.Events
{
    public class EventManager : MonoBehaviour
    {

        private Dictionary<string, UnityEvent> eventDictionary;
        private Dictionary<string, UnityEventOneArg> oneArgEventDictionary;
        private Dictionary<string, UnityEventTwoArg> twoArgEventDictionary;
        private Dictionary<string, UnityEventThreeArg> threeArgEventDictionary;
        private Dictionary<string, UnityEventFourArg> fourArgEventDictionary;
        private static EventManager eventManager;

        public static EventManager instance
        {
            get
            {
                if (!eventManager)
                {
                    eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;
                    if (!eventManager)
                    {
                        Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                    }
                    else
                    {
                        eventManager.Init();
                    }
                }
                return eventManager;
            }
        }

        void Init()
        {
            if (eventDictionary == null)
            {
                eventDictionary = new Dictionary<string, UnityEvent>();
                oneArgEventDictionary = new Dictionary<string, UnityEventOneArg>();
                twoArgEventDictionary = new Dictionary<string, UnityEventTwoArg>();
                threeArgEventDictionary = new Dictionary<string, UnityEventThreeArg>();
                fourArgEventDictionary = new Dictionary<string, UnityEventFourArg>();
            }
        }

        public static void StartListening(string eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEvent();
                thisEvent.AddListener(listener);
                instance.eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StartListening(string eventName, UnityAction<object> listener)
        {
            UnityEventOneArg thisEvent = null;
            if (instance.oneArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEventOneArg();
                thisEvent.AddListener(listener);
                instance.oneArgEventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StartListening(string eventName, UnityAction<object, object> listener)
        {
            UnityEventTwoArg thisEvent = null;
            if (instance.twoArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEventTwoArg();
                thisEvent.AddListener(listener);
                instance.twoArgEventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StartListening(string eventName, UnityAction<object, object, object> listener)
        {
            UnityEventThreeArg thisEvent = null;
            if (instance.threeArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEventThreeArg();
                thisEvent.AddListener(listener);
                instance.threeArgEventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StartListening(string eventName, UnityAction<object, object, object, object> listener)
        {
            UnityEventFourArg thisEvent = null;
            if (instance.fourArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEventFourArg();
                thisEvent.AddListener(listener);
                instance.fourArgEventDictionary.Add(eventName, thisEvent);
            }
        }


        public static void StopListening(string eventName, UnityAction listener)
        {
            if (eventManager == null) return;
            UnityEvent thisEvent = null;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void StopListening(string eventName, UnityAction<object> listener)
        {
            if (eventManager == null) return;
            UnityEventOneArg thisEvent = null;
            if (instance.oneArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void StopListening(string eventName, UnityAction<object, object> listener)
        {
            if (eventManager == null) return;
            UnityEventTwoArg thisEvent = null;
            if (instance.twoArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void StopListening(string eventName, UnityAction<object, object, object> listener)
        {
            if (eventManager == null) return;
            UnityEventThreeArg thisEvent = null;
            if (instance.threeArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void StopListening(string eventName, UnityAction<object, object, object, object> listener)
        {
            if (eventManager == null) return;
            UnityEventFourArg thisEvent = null;
            if (instance.fourArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void TriggerEvent(string eventName)
        {
            UnityEvent thisEvent = null;
            if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke();
            }
        }

        public static void TriggerEvent(string eventName, object arg0)
        {
            UnityEventOneArg thisEvent = null;
            if (instance.oneArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke(arg0);
            }
        }

        public static void TriggerEvent(string eventName, object arg0, object arg1)
        {
            UnityEventTwoArg thisEvent = null;
            if (instance.twoArgEventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke(arg0, arg1);
            }
        }

        public void printAllEvents()
        {
            foreach (string s in eventDictionary.Keys)
            {
                DebugLogger.instance.logInfo("0 Arg Event: " + s, GetHashCode(), GetType(), gameObject.name);
            }

            foreach (string s in oneArgEventDictionary.Keys)
            {
                DebugLogger.instance.logInfo("1 Arg Event: " + s, GetHashCode(), GetType(), gameObject.name);
            }

            foreach (string s in twoArgEventDictionary.Keys)
            {
                DebugLogger.instance.logInfo("2 Arg Event: " + s, GetHashCode(), GetType(), gameObject.name);
            }

            foreach (string s in threeArgEventDictionary.Keys)
            {
                DebugLogger.instance.logInfo("3 Arg Event: " + s, GetHashCode(), GetType(), gameObject.name);
            }

            foreach (string s in fourArgEventDictionary.Keys)
            {
                DebugLogger.instance.logInfo("4 Arg Event: " + s, GetHashCode(), GetType(), gameObject.name);
            }

        }
    }
}