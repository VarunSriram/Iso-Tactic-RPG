﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Game.Events
{
    public abstract class GameEventOneArg : MonoBehaviour
    {
        [SerializeField]
        protected UnityAction<object> listener;
        protected abstract void eventAction(object arg0);
        protected string eventName;

        public void registerEvents()
        {
            listener = new UnityAction<object>(eventAction);
            EventManager.StartListening(this.eventName, listener);
        }

        public void deRegisterEvents()
        {
            EventManager.StopListening(this.eventName, listener);
        }

    }
}