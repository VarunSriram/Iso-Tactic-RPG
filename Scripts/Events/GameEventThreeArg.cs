﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Game.Events
{
    public abstract class GameEventThreeArg : MonoBehaviour
    {
        [SerializeField]
        protected UnityAction<object, object, object> listener;
        protected abstract void eventAction(object arg0, object arg1, object arg2);
        protected string eventName;

        public void registerEvents()
        {
            listener = new UnityAction<object, object, object>(eventAction);
            EventManager.StartListening(this.eventName, listener);
        }

        public void deRegisterEvents()
        {
            EventManager.StopListening(this.eventName, listener);
        }
    }
}