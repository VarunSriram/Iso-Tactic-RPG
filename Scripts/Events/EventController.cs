﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Events
{
    public abstract class EventController : MonoBehaviour
    {
        [SerializeField]
        protected List<GameEvent> gameEvents;
        [SerializeField]
        protected List<GameEventOneArg> gameEventsOneArg;
        [SerializeField]
        protected List<GameEventTwoArg> gameEventsTwoArg;

        protected void registerAllEvents()
        {
            foreach (GameEvent ge in gameEvents)
            {
                ge.registerEvents();
            }
            foreach (GameEventOneArg ge in gameEventsOneArg)
            {
                ge.registerEvents();
            }
            foreach (GameEventTwoArg ge in gameEventsTwoArg)
            {
                ge.registerEvents();
            }
        }

        public void deRegisterAllEvents()
        {
            foreach (GameEvent ge in gameEvents)
            {
                ge.deRegisterEvents();
            }
            foreach (GameEventOneArg ge in gameEventsOneArg)
            {
                ge.deRegisterEvents();
            }
            foreach (GameEventTwoArg ge in gameEventsTwoArg)
            {
                ge.deRegisterEvents();
            }
        }

        protected void populateGameEvents()
        {
            gameEvents = new List<GameEvent>(GetComponents<GameEvent>());
            gameEventsOneArg = new List<GameEventOneArg>(GetComponents<GameEventOneArg>());
            gameEventsTwoArg = new List<GameEventTwoArg>(GetComponents<GameEventTwoArg>());
        }

        public virtual void initializeEvents()
        {
            this.populateGameEvents();
            this.registerAllEvents();
        }
    }
}