﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Game.Events
{
    public abstract class GameEvent : MonoBehaviour
    {
        [SerializeField]
        protected UnityAction listener;
        protected abstract void eventAction();
        protected string eventName;
        public void registerEvents()
        {
            listener = new UnityAction(eventAction);
            EventManager.StartListening(this.eventName, listener);
        }

        public void deRegisterEvents()
        {
            EventManager.StopListening(this.eventName, listener);
        }
    }
}