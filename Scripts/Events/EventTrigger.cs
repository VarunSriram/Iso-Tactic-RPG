﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.Events
{
    public interface EventTrigger
    {

        void triggerEvent();
    }
}