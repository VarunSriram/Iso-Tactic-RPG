﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Game.Events
{
    [System.Serializable]
    public class UnityEventOneArg : UnityEvent<object>
    {

    }
}