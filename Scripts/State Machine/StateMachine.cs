﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.StateMachines
{
    public abstract class StateMachine
    {
        protected State currentState;

        public void setState(State newState)
        {
            this.currentState.onStateExit(this);
            this.currentState = newState;
            this.currentState.onStateEnter(this);
        }

        public State getCurrentState()
        {
            return currentState;
        }

        public abstract void initialize();
     
    }

}