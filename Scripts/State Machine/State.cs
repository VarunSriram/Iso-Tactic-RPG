﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game.StateMachines
{
    public abstract class State
    {
        public abstract void onStateEnter(StateMachine fsm);
        public abstract void onStateExit(StateMachine fsm);
    }
}